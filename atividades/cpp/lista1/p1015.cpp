#include <iostream>
#include <iomanip>
#include <cmath>

using namespace std;

int main(){
    float x1, y1;
    cin >> x1 >> y1;

    float x2, y2;   
    cin >> x2 >> y2;

    float res = sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));

    // [setprecision] modifica a precisão do valor
    cout << fixed;
    cout << setprecision(4) << res << endl;

    return 0;
}