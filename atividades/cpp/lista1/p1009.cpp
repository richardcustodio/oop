#include <iostream>
#include <iomanip>

using namespace std;

int main(){
    char nome[100];
    double salario, vendas;
    cin >> nome;
    cin >> salario;
    cin >> vendas;

    double total = salario + (vendas * 0.15);
    cout << fixed;
    cout << setprecision(2) << "TOTAL = R$ " << total << endl;

    return 0;
}