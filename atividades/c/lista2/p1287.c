#include <stdio.h>
#include <string.h>

int main(){
    int n, m = 0, f = 0;
    char nome[50][50];
    do{
      scanf("%d", &n);
      if (f == 1 && n != 0){
        printf("\n");
      }

      for (int i=0; i<n; i++){
          scanf("%s", nome[i]);
          if (strlen(nome[i]) > m){
            m = strlen(nome[i]);
          }
      }

      for (int i=0; i<n; i++){
        printf("%*s\n", m, nome[i]);
      }
      m = 0;
      f = 1;
    }while (n != 0);

    return 0;
}
