#include <stdio.h>

int main(){
    int t;
    int pa[3000], pb[3000];
    float g1[3000], g2[3000];

    scanf("%d", &t);
    for (int i=0; i<t; i++){
      scanf("%d%d%f%f", &pa[i], &pb[i], &g1[i], &g2[i]);
    }

    for (int i=0; i<t; i++){
      int y = 1, ta = pa[i], tb = pb[i];
      while (1){
        ta += ta * (g1[i]/100);
        tb += tb * (g2[i]/100);
        // printf("i: %d ta: %d tb: %d\n", i, ta, tb);

        if (ta > tb){
          printf("%d anos.\n", y);
          break;
        }
        y += 1;

        if (y > 100){
          printf("Mais de 1 seculo.\n");
          break;
        }
      }
    }

    return 0;
}
