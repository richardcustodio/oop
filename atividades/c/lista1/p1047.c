#include <stdio.h>

int main(){
    int h1, m1, h2, m2;
    scanf("%d%d%d%d", &h1, &m1, &h2, &m2);

    if (h1 == h2 && m1 == m2){
      printf("O JOGO DUROU 24 HORA(S) E 0 MINUTO(S)\n");
      return 0;
    }

    int hr = h2 - h1;
    int mr = m2 - m1;

    if (hr < 0){
      hr += 24;
    }
    if (mr < 0){
      mr += 60;
      hr -= 1;
      if (hr < 0){
        hr += 24;
      }
    }

    printf("O JOGO DUROU %d HORA(S) E %d MINUTO(S)\n", hr, mr);

    return 0;
}
