#include <stdio.h>

int main(){
    double a, b, c;
    scanf("%lf%lf%lf", &a, &b, &c);

    double tmp;
    if (b > a){
      tmp = a;
      a = b;
      b = tmp;
    }
    if (c > b){
      tmp = c;
      c = b;
      b = tmp;
    }
    if (b > a){
      tmp = a;
      a = b;
      b = tmp;
    }

    double a2 = a*a;
    double b2 = b*b;
    double c2 = c*c;

    if (a >= b + c){
      printf("NAO FORMA TRIANGULO\n");
      return 0;
    }

    if (a2 == b2 + c2){
      printf("TRIANGULO RETANGULO\n");
    }else if (a2 > b2+c2){
      printf("TRIANGULO OBTUSANGULO\n");
    }else if (a2 < b2 + c2){
      printf("TRIANGULO ACUTANGULO\n");
    }

    if(a == b && b == c){
      printf("TRIANGULO EQUILATERO\n");
    }else if (a == b || a == c || b == c){
      printf("TRIANGULO ISOSCELES\n");
    }

    return 0;
}
