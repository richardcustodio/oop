#include <stdio.h>
#include <stdlib.h>

int main(){
    int a, b;
    scanf("%d%d", &a, &b);

    int r = a % b;
    if (r < 0 ){
      r += abs(b);
    }

    int q = (a - r) / b;
    printf("%d %d\n", q, r);

    return 0;
}
