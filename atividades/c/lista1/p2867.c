#include <stdio.h>
#include <math.h>

int main(){
    int t;
    int c[30000], m[30000];

    scanf("%d", &t);
    for (int i=0; i<t; i++){
      scanf("%d%d", &c[i], &m[i]);
    }

    for (int i=0; i<t; i++){
      int n = 1 + floor(m[i] * log10(c[i]));
      printf("%d\n", n);
    }

    return 0;
}
