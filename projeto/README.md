# Robot Simulator
Simulação de robô feito em C++ com as bibliotecas SDL2 e OpenGL.

#### Objetivos
- Modelar robô com motores (com eficiência), caixas de redução (com eficiência), rodas

- Movimentação dos robôs será via ajustes de velocidade de motores

- Robôs poderão ter formas retângulares, quadradas ou circulares

- Permitir o posicionamento do eixo das rodas

- Desenhar trajetória do movimento do Robô

- Interface em OpenGL.

- Comunicação TCP/IP para envio dos comandos de velocidade dos motores.

## Instruções

### Dependências
- CMake
- SDL2
- OpenGL

### Buildando
```bash
> mkdir build
> cd build
> cmake ..
> make
```
### Executando
```bash
> ./main
```
