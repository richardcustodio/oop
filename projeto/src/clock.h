#ifndef CLOCK_H
#define CLOCK_H

#include <cstdint>

/// Essa classe é responsável por controlar o tempo da simulação.
///
/// Esta classe permite que simulação continue estável e consistente até mesmo em computadores com _hardware_ completamente diferente,
/// seu funcionamento é baseado na utilização da diferença entre o tempo que a Simulação leva para completar um Simulation::tick.
class Clock
{
public:
    /// Construtor padrão da classe Clock.
    /// @param ups Parâmetro que representa o número de atualizações por segundo desejado (_updates per second_).
    /// @warning Esta classe não garante que a simulação irá realizar exatamente o número de atualizações por segundo especificado.
    Clock(uint64_t ups);

    /// Metódo que retorna o _delta_ atual do Clock.
    /// @note Essa é a *unica* forma de acessar um atributo desta classe.
    double get_delta();

    /// Esse metódo é responsável pela redefinição de alguns valores do Clock, indicando um novo _tick_.
    /// @see Simulation::tick
    void reset();
    /// Esse metódo é responsável pela atualização dos valores, indicando que um Simulation::update está sendo realizado.
    void update();
    /// Esse metódo é responsável por retornar se a simulação deve ser atualizada ou não.
    /// @see Simulation::update
    bool should_update();

private:
    /// Atributo responsável por armazenar o último instante em que _tick_ foi realizado.
    /// @see Simulation::tick
    uint64_t current;
    /// Atributo responsável por armazenar o número de atualizações por segundo desejadas.
    double m_ups;
    /// Atributo responsável por armazenar o _lag_ atual.
    double accumulator;
    /// Atributo responsável por armazenar o _delta_ atual, ou seja, a diferença entre o tempo do último _tick_ e o tempo atual.
    /// @see Simulation::tick, Simulation::update
    double delta;
};

#endif