#include "simulation.h"

#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/vector_angle.hpp>

Simulation::Simulation(int width, int height, uint64_t ups, glm::vec3 p_destination) : timer{ups}, renderer{width, height}
{
    destination = p_destination;

    std::ifstream file("robot.txt");
    std::string line;
    if (file.is_open()){
        std::getline(file, line);
        std::cout << line << '\n';

        std::istringstream in(line);
        float x, y;
        int size;
        in >> x >> y >> size;
        robot = Robot(glm::vec3(x, y, 0.0f), glm::vec3(0, 0, 0), size);
    }else{
        robot = Robot();
    }
}

void Simulation::run()
{
    this->renderer.setup();

    while (this->running)
    {
        this->tick();
    }
}

void Simulation::tick()
{
    this->timer.reset();

    this->handle_events();

    while (this->timer.should_update())
    {
        this->update();
        this->timer.update();
    }

    this->render();
}

void Simulation::handle_events()
{
    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_MOUSEBUTTONDOWN:
            switch (event.button.button)
            {
            case SDL_BUTTON_LEFT:
                int mouse_x, mouse_y;
                SDL_GetMouseState(&mouse_x, &mouse_y);
                this->destination = glm::vec3(mouse_x, mouse_y, 0);
                break;
            }
            break;
        case SDL_KEYUP:
            if (event.key.repeat != 0)
                break;
            switch (event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                this->running = false;
                break;
            }
            break;
        case SDL_QUIT:
            this->running = false;
            break;
        }
    }
}
void Simulation::update()
{
    float distance = glm::distance(this->robot.m_position, this->destination);

    /// distância unitária
    glm::vec3 ud = glm::normalize(this->destination - this->robot.m_position);
    if (round(distance) == 0.0f)
    {
        this->robot.m_velocity = glm::vec3(0, 0, 0);
    }
    else
    {
        this->robot.m_velocity = ud * 0.05f;
    }

    this->robot.m_position += this->robot.m_velocity * (float)this->timer.get_delta();
}

void Simulation::render()
{
    this->renderer.start();

    // Draw robot
    glm::mat4 model = glm::mat4(1.0f);
    model = glm::translate(model, this->robot.m_position);
    model = glm::scale(model, glm::vec3(this->robot.m_size, this->robot.m_size, 0));

    Quad quad = Quad(model, glm::vec3(1, 0, 0));
    this->renderer.draw(&quad);

    // Draw destination line    
    glm::vec3 vec = this->destination - this->robot.m_position;

    model = glm::mat4(1.0f);
    model = glm::translate(model, this->robot.m_position);
    model = glm::rotate(model, glm::atan(vec.y, vec.x), glm::vec3(0, 0, 1));

    float distance = glm::length(vec);
    model = glm::scale(model, glm::vec3(distance, 0.0f, 0.0f));

    Line line = Line(model, glm::vec3(1, 1, 1));
    this->renderer.draw(&line);

    // Draw destination point
    model = glm::mat4(1.0f);
    model = glm::translate(model, this->destination);
    model = glm::scale(model, glm::vec3(5, 5, 0));

    Circle circle = Circle(model, glm::vec3(1, 1, 1));
    this->renderer.draw(&circle);

    this->renderer.end();
}
