#include "simulation.h"

int main(int argc, char const *argv[])
{
  SDL_Init(SDL_INIT_VIDEO);
  SDL_GL_SetSwapInterval(1);

  Simulation simulation = Simulation(800, 600, 20);
  simulation.run();

  return 0;
}
