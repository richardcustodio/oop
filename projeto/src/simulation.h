#ifndef SIMULATION_H
#define SIMULATION_H

#include "clock.h"
#include "renderer.h"
#include "robot.h"

#include <string>
#include <sstream>
#include <fstream>

/// Essa classe agrega todas as outras classes, ela serve como a classe responsável por controlar toda a simulação, simula o robô e utiliza o Renderer para apresentá-lo.
///
/// O Simulador é o coração do projeto, ele é responsável por controlar tudo, desde o robô até o estado do renderizador.
/// Através da utilização de outras classes auxiliares, o simulador mantém o estado dos robôs consistente e fluído,
/// permitindo uma melhor visualização da simulação.
class Simulation
{
public:
    /// Construtor padrão para classe Simulation.
    /// @param width Parâmetro que representa a largura da janela.
    /// @param height Parâmetro que representa a altura da janela.
    /// @param ups Parâmetro que representa o número de atualizações por segundo desejado (updates per second).
    /// @param p_destination Parâmetro que representa o destino desejado do robô (inicializado como (0, 0, 0) por padrão).
    Simulation(int width, int height, uint64_t ups, glm::vec3 p_destination = glm::vec3(0.0f, 0.0f, 0.0f));

    /// Inicia e mantém a simulação ativa enquanto necessário.
    /// @note Essa é a única função pública, e a única necessária para manter a simulação ativa.
    void run();

private:
    /// Atributo que contém uma instância da classe Clock, responsável por manter a velocidade da simulação estável em qualquer situação.
    Clock timer;
    /// Atributo que contém uma instância da classe Renderer, responsável pela interface com o OpenGl e apresentar os quadros na janela.
    Renderer renderer;
    /// Atributo que contém uma instância da classe SDL_Event, responsável por amazenar cada evento capturado pelo SDL.
    SDL_Event event;
    /// Atributo booleano, responsável por sinalizar a simulação se deve permanecer ativa ou não.
    bool running = true;

    /// Atributo que contém um instância da classe Robot, representa a posição, velocidade e cor do robô na tela.
    Robot robot;

    /// Atributo que contém um instância da classe glm::vec3, representa o destino desejado do robô.
    glm::vec3 destination;

    /// Esse metódo é responsável por realizar uma única atualização da simulação, também conhecido como _tick_.
    void tick();

    /// Esse metódo é responsável pelo tratamento dos eventos capturados pela janela.
    void handle_events();

    /// Esse metódo é responsável por mover a simulação (atualizando as posições, etc).
    void update();

    /// Esse metódo é responsável por rasterizar a simulação em um quadro e apresentá-lo na janela.
    void render();
};

#endif