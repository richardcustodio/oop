#include "clock.h"
#include <SDL2/SDL.h>

Clock::Clock(uint64_t ups)
{
    m_ups = (double)ups;
    current = SDL_GetPerformanceCounter();
    accumulator = 0.0f;
    delta = 0.0f;
}

void Clock::reset()
{
    uint64_t now = SDL_GetPerformanceCounter();
    this->delta = (double)((now - this->current) * 1000 / (double)SDL_GetPerformanceFrequency());
    this->current = now;
    this->accumulator += this->delta;
}

void Clock::update()
{
    this->accumulator -= this->delta;
}

bool Clock::should_update()
{
    return this->accumulator >= this->delta;
}

double Clock::get_delta()
{
    return this->delta;
}