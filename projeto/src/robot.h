#ifndef ROBOT_H
#define ROBOT_H

#include <vector>
#include <glm/glm.hpp>

/// Essa classe é responsável por representar o robô.
/// 
/// Robot é uma das principais classes, através desta classe o robô é especificado,
/// contendo um formato, posição e velocidade, que são utilizados para simular a movimentação do robô.
class Robot
{
public:
    /// Construtor padrão para classe Robot.
    /// @param position Parâmetro que representa a posição do robô (inicializado como (400, 300, 0) por padrão).
    /// @param velocity Parâmetro que representa a velocidade do robô (inicializado como (0,0,0) por padrão).
    /// @param size Parâmetro que representa o tamanho do robô (inicializado como 25 por padrão).
    Robot(glm::vec3 position = glm::vec3(400.0f, 300.0f, 0.0f), glm::vec3 velocity = glm::vec3(0, 0, 0), int size = 25);

    /// Atributo que contém uma instância da classe glm::vec3, responsável por armazenar a posição do robô.
    glm::vec3 m_position;
    /// Atributo que contém uma instância da classe glm::vec3, responsável por armazenar a velocidade do robô.
    glm::vec3 m_velocity;
    /// Atributo que contém um inteiro, responsável por armazenar o tamanho do robô.
    int m_size;
};

#endif