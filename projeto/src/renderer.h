#ifndef RENDERER_H
#define RENDERER_H

#define GL_GLEXT_PROTOTYPES

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

class Shape;

/// Essa classe é responsável por manter os estados necessários para a apresentação da simulação na tela.
///
/// Interage com o SDL que possibilita a criação de gráficos na tela, e com o OpenGL que possui comunicação com a placa de vídeo.
class Renderer
{
public:
    /// Construtor padrão da classe Renderer.
    /// @param windowWidth Parâmetro que representa a largura da janela.
    /// @param windowHeight Parâmetro que representa a altura da janela.
    Renderer(int windowWidth, int windowHeight);

    /// Metódo responsável por inicializar os atributos necessários para a _interface_ com o OpenGL.
    void setup();

    /// Metódo responsável por indicar o início de um novo _frame_.
    void start();

    /// Metódo responsável por desenhar um formato arbitrário no _frame_.
    /// @see Shape
    void draw(Shape *shape);

    /// Metódo responsável por indicar o fim de um _frame_.
    void end();

    /// Estes atributos representam os _buffers_ de memória alocados no dispositivo de vídeo.
    unsigned int VAO = 1, QUAD_VBO = 2, CIRCLE_VBO = 3, LINE_VBO = 10, QUAD_EBO = 4;

private:
    /// Atributos que representam o tamanho da janela.
    int m_windowWidth, m_windowHeight;
    /// Atributo que representa um ponteiro para uma janela.
    SDL_Window *window;
    /// Atributo que contém uma classe SDL_GLContext, responsável por controlar a _state machine_ da interface com o OpenGL.
    SDL_GLContext context;

    /// Atributo que representa o _program_ utilizado (ou seja, o conjunto de _fragment_ e _vertex_ shader).
    unsigned int shaderProgram = 5;
    /// Atributos que representam a localização dos _uniforms_ utilizados no _shader program_.
    unsigned int modelLocation = 6, viewLocation = 7, projectionLocation = 8, colorLocation = 9;

    /// Atributos que representam as matrizes utilizadas para a visualização da simulação.
    glm::mat4 view, projection;
};

/// Essa classe é responsável por abstrair o formato e cor do robô, permitindo flexibilidade na sua rasterização.
class Shape
{
public:
    /// Construtor padrão para classe Shape.
    /// @param p_model Parâmetro que representa a matriz _model_.
    /// @param p_color Parâmetro que representa a cor do formato.
    Shape(glm::mat4 p_model, glm::vec3 p_color);

    /// Metódo virtual, responsável por desenhar o formato no _frame_ do Renderer.
    /// @param renderer Parâmetro que representa um ponteiro para um Renderer.
    virtual void draw(Renderer *renderer) = 0;

    /// Metódo que retorna o _model_ do formato.
    glm::mat4 get_model()
    {
        return model;
    };

    /// Metódo que retorna o _color_ do formato.
    glm::vec3 get_color()
    {
        return color;
    };

protected:
    /// Atributo que representa a matriz _model_ do formato.
    glm::mat4 model;
    /// Atributo que representa a cor do formato.
    glm::vec3 color;
};

/// Essa classe representa um retângulo (_quad_) através da abstração de Shape.
class Quad : public Shape
{
public:
    /// Construtor padrão para Quad.
    /// @param p_model Representa a matriz _model_ do retângulo.
    /// @param p_color Representa a cor do retângulo.
    Quad(glm::mat4 p_model, glm::vec3 p_color) : Shape(p_model, p_color) {}

    /// Metódo responsável por desenhar o retângulo.
    void draw(Renderer *renderer);
};

/// Essa classe representa um círculo (_circle_) através da abstração de Shape.
class Circle : public Shape
{
public:
    /// Construtor padrão para Circle.
    /// @param p_model Representa a matriz _model_ do círculo.
    /// @param p_color Representa a cor do círculo.
    Circle(glm::mat4 p_model, glm::vec3 p_color) : Shape(p_model, p_color) {}

    /// Metódo responsável por desenhar o círculo.
    void draw(Renderer *renderer);
};

/// Essa classe representa uma linha (_line_) através da abstração de Shape.
class Line : public Shape
{
public:
    /// Construtor padrão para Line.
    /// @param p_model Representa a matriz _model_ da linha.
    /// @param p_color Representa a cor da linha.
    Line(glm::mat4 p_model, glm::vec3 p_color) : Shape(p_model, p_color) {}

    /// Metódo responsável por desenhar a linha.
    void draw(Renderer *renderer);
};

#endif