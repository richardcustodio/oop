#include "renderer.h"

Renderer::Renderer(int windowWidth, int windowHeight)
{
    m_windowWidth = windowWidth;
    m_windowHeight = windowHeight;

    window = SDL_CreateWindow(
        "Robot Simulator", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        m_windowWidth, m_windowHeight, SDL_WINDOW_OPENGL);

    context = SDL_GL_CreateContext(window);
}

void Renderer::setup()
{
    // ============ SHADERS ============

    std::string vertexCode;
    std::string fragmentCode;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    // ensure ifstream objects can throw exceptions:
    vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        // open files
        vShaderFile.open("../shaders/vertex.glsl");
        fShaderFile.open("../shaders/fragment.glsl");
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        // close file handlers
        vShaderFile.close();
        fShaderFile.close();
        // convert stream into string
        vertexCode = vShaderStream.str();
        fragmentCode = fShaderStream.str();
    }
    catch (std::ifstream::failure e)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
    }

    const char *vertexShaderSource = vertexCode.c_str();
    const char *fragmentShaderSource = fragmentCode.c_str();

    unsigned int vertexShader;
    vertexShader = glCreateShader(GL_VERTEX_SHADER);

    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);

    unsigned int fragmentShader;
    fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);

    shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // ============ BUFFERS ============
    // Allocate & Generate
    glGenVertexArrays(1, &VAO);

    // Bind
    glBindVertexArray(VAO);

    // -------- CIRCLE BUFFER --------
    float radius = 1.0f;
    std::vector<float> circleVertices = {};

    for (float angle = glm::radians(0.0f); angle <= glm::radians(360.0f); angle += glm::radians(10.0f))
    {
        float vertex[] = {
            radius * cos(angle), radius * sin(angle), 0.0f};
        circleVertices.insert(std::end(circleVertices), std::begin(vertex), std::end(vertex));
    }
    // std::cout << "Size: " << circleVertices.size() << std::endl;

    glGenBuffers(1, &CIRCLE_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, CIRCLE_VBO);
    glBufferData(GL_ARRAY_BUFFER, circleVertices.size() * sizeof(float), &circleVertices[0], GL_STATIC_DRAW);

    // -------- QUAD BUFFER --------
    float quadVertices[] = {
        0.5f, 0.5f, 0.0f,   // top right
        0.5f, -0.5f, 0.0f,  // bottom right
        -0.5f, -0.5f, 0.0f, // bottom left
        -0.5f, 0.5f, 0.0f   // top left
    };
    unsigned int quadIndices[] = {
        0, 1, 3, // first triangle
        1, 2, 3  // second triangle
    };

    glGenBuffers(1, &QUAD_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, QUAD_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), quadVertices, GL_STATIC_DRAW);

    glGenBuffers(1, &QUAD_EBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, QUAD_EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(quadIndices), quadIndices, GL_STATIC_DRAW);

    // -------- QUAD BUFFER --------
    float lineVertices[] = {
        0.0f,
        0.0f,
        0.0f,
        1.0f,
        1.0f,
        0.0f,
    };

    glGenBuffers(1, &LINE_VBO);
    glBindBuffer(GL_ARRAY_BUFFER, LINE_VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertices), lineVertices, GL_STATIC_DRAW);

    // Unbind
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);

    modelLocation = glGetUniformLocation(shaderProgram, "model");
    viewLocation = glGetUniformLocation(shaderProgram, "view");
    projectionLocation = glGetUniformLocation(shaderProgram, "projection");

    colorLocation = glGetUniformLocation(shaderProgram, "uColor");

    view = glm::lookAt(
        glm::vec3(0, 0, 1),
        glm::vec3(0, 0, 0),
        glm::vec3(0, 1, 0));

    projection = glm::ortho(0.0f, (float)m_windowWidth, (float)m_windowHeight, 0.0f, -1.0f, 1.0f);
}

void Renderer::start()
{
    glViewport(0, 0, m_windowWidth, m_windowHeight);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(shaderProgram);

    glUniformMatrix4fv(viewLocation, 1, GL_FALSE, glm::value_ptr(view));
    glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, glm::value_ptr(projection));
}

void Renderer::draw(Shape *shape)
{
    glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(shape->get_model()));
    glUniform3fv(colorLocation, 1, glm::value_ptr(shape->get_color()));

    glBindVertexArray(VAO);

    shape->draw(this);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void Renderer::end()
{
    SDL_GL_SwapWindow(window);
}

Shape::Shape(glm::mat4 p_model, glm::vec3 p_color)
{
    model = p_model;
    color = p_color;
}

void Quad::draw(Renderer *renderer)
{
    glBindBuffer(GL_ARRAY_BUFFER, renderer->QUAD_VBO);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, renderer->QUAD_EBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void Circle::draw(Renderer *renderer)
{
    glBindBuffer(GL_ARRAY_BUFFER, renderer->CIRCLE_VBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);

    glDrawArrays(GL_TRIANGLE_FAN, 0, 111);
}

void Line::draw(Renderer *renderer)
{
    glBindBuffer(GL_ARRAY_BUFFER, renderer->LINE_VBO);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(0);

    glDrawArrays(GL_LINES, 0, 2);
}